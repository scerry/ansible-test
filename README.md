# Ansible Service

Overview
--------

Ansible Service to deploy Nginx or Apache2 Web Service.

To deploy:
----------

    ansible-playbook playbooks/install_web.yml -i inventory/hosts

Variables:
----------

Apache2 (Mandatory):

    web_package="apache2"

Nginx (Mandatory):

    web_package="nginx"

Web Port (Not Mandatory):

    web_port=8080
