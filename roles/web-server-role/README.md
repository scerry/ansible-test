# web-server-role

Overview
--------

Ansible role to install Nginx or Apache2 and configure service.

If for any reason Nginx or Apache2 fails to restart a backup will be restored.

Can be run with or without port, automation gets current web Port and compare to web_port if defined.

Automation can be re-run with a different port at any moment.

Role Variables
--------------

    web_package="apache2" or web_package="nginx"
    web_port=8080

Dependencies
------------

No role dependencies.

Example Playbook
----------------

    - hosts: web
      roles:
        - { role: web-server-role }

Example Role Requirements
-------------------------

    - src: "git@gitlab.com:scerry/ansible-test.git"
      scm: git
      version: master
